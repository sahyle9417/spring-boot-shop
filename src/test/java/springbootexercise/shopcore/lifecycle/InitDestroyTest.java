package springbootexercise.shopcore.lifecycle;

import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

public class InitDestroyTest {

    @Test
    public void initDestroyTest() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(InitDestroyConfig.class);
        NetworkClient networkClient = applicationContext.getBean(NetworkClient.class);
        applicationContext.close();
    }

    @Configuration
    static class InitDestroyConfig {

        // @Bean(initMethod = "init", destroyMethod = "destroy")
        @Bean
        public NetworkClient networkClient() {
            NetworkClient networkClient = new NetworkClient();
            networkClient.setUrl("http://hello-spring.com");
            return networkClient;
        }
    }
}

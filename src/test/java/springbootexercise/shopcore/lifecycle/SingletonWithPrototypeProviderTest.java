package springbootexercise.shopcore.lifecycle;

import lombok.Getter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class SingletonWithPrototypeProviderTest {

    @Test
    void prototypeTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(PrototypeBean.class);

        // Prototype Bean1 addCount 실행
        // Bean1 Count 는 초기값인 0에서 1로 증가
        PrototypeBean prototypeBean1 = applicationContext.getBean(PrototypeBean.class);
        prototypeBean1.addCount();
        Assertions.assertThat(prototypeBean1.getCount()).isEqualTo(1);

        // Prototype Bean1 과 Prototype Bean2 는 완전 별개의 Bean
        // Bean2 Count 는 초기값인 0
        PrototypeBean prototypeBean2 = applicationContext.getBean(PrototypeBean.class);
        Assertions.assertThat(prototypeBean2.getCount()).isEqualTo(0);

        // PreDestroy 어노테이션 지정해도 prototype bean 자동 destroy 되지 않음
        // 아래와 같이 명시적으로 destroy 해야 함
        prototypeBean1.destroy();
        prototypeBean2.destroy();
    }

    @Test
    void singletonWithPrototypeTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(PrototypeBean.class, SingletonBean.class);

        SingletonBean singletonBean1 = applicationContext.getBean(SingletonBean.class);
        // 옳은 예시 (singleton Bean 에 속해 있지만 제대로 prototype 처럼 동작, 0 -> 1)
        int count1 = singletonBean1.logic();
        Assertions.assertThat(count1).isEqualTo(1);

        SingletonBean singletonBean2 = applicationContext.getBean(SingletonBean.class);
        // 옳은 예시 (singleton Bean 에 속해 있지만 제대로 prototype 처럼 동작, 0 -> 1)
        int count2 = singletonBean2.logic();
        Assertions.assertThat(count2).isEqualTo(1);

    }

    @Test
    void singletonWithPrototypeWrongTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(PrototypeBean.class, WrongSingletonBean.class);

        WrongSingletonBean wrongSingletonBean1 = applicationContext.getBean(WrongSingletonBean.class);
        // 잘못된 예시 (prototype 이지만 singleton 처럼 동작, 0 -> 1)
        int count1 = wrongSingletonBean1.logic();
        Assertions.assertThat(count1).isEqualTo(1);

        WrongSingletonBean wrongSingletonBean2 = applicationContext.getBean(WrongSingletonBean.class);
        // 잘못된 예시 (prototype 이지만 singleton 처럼 동작, 0 -> 1)
        int count2 = wrongSingletonBean2.logic();
        Assertions.assertThat(count2).isEqualTo(2);

    }

    @Scope("prototype")
    static class PrototypeBean {
        @Getter
        private int count = 0;

        public void addCount() {
            count++;
        }

        @PostConstruct
        public void init() {
            System.out.println("Prototype.init");
        }

        // prototype bean 이므로 스프링에 의해 자동으로 실행되진 않는다.
        @PreDestroy
        public void destroy() {
            System.out.println("Prototype.destroy");
        }

    }

    @Scope("singleton")
    static class SingletonBean {

        /*
         * ObjectProvider (권장)
         * - 스프링 컨테이너에서 지정된 Type 의 Bean 찾아서 반환해주는 역할(DL, Dependency Lookup) 수행
         * - 스프링에 의존적 (javax.inject.Provider 와 비교되는 지점)
         * - 기능이 좀 더 풍부 (javax.inject.Provider 대비)
         * - 스프링과 자바 표준 기능 충돌 시 스프링 기능 권장 (스프링이 자바 표준보다 메인)
         *
         * Provider
         * - 컨테이너에서 지정된 Type 의 Bean 찾아서 반환해주는 역할(DL, Dependency Lookup) 수행
         * - 스프링과 무관한 자바 표준 (ObjectProvider 대비 장점)
         * - 기능이 좀 더 단순 (ObjectProvider 대비)
         */

        private final ObjectProvider<PrototypeBean> prototypeBeanObjectProvider;
        @Autowired
        public SingletonBean(ObjectProvider<PrototypeBean> prototypeBeanObjectProvider) {
            this.prototypeBeanObjectProvider = prototypeBeanObjectProvider;
        }

        /*
        private final Provider<PrototypeBean> prototypeBeanProvider;
        @Autowired
        public SingletonBean(Provider<PrototypeBean> prototypeBeanProvider) {
            this.prototypeBeanProvider = prototypeBeanProvider;
        }
        */

        public int logic() {
            PrototypeBean prototypeBean = prototypeBeanObjectProvider.getObject();
            // PrototypeBean prototypeBean = prototypeBeanProvider.get();
            prototypeBean.addCount();
            return prototypeBean.getCount();
        }

    }

    @Scope("singleton")
    static class WrongSingletonBean {
        // Prototype Bean 임에도 Singleton Bean 에 소속되어 있으므로 Singleton Bean 처럼 동작
        private final PrototypeBean prototypeBean;

        @Autowired
        public WrongSingletonBean(PrototypeBean prototypeBean) {
            this.prototypeBean = prototypeBean;
        }

        public int logic() {
            prototypeBean.addCount();
            return prototypeBean.getCount();
        }

    }

}

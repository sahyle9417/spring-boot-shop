package springbootexercise.shopcore.lifecycle;

import lombok.Setter;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/*
 * Bean 생성 후 호출 메서드(Init), 종료 전 호출 메서드(Destroy) 설정 방법 3가지
 * 
 * 1. PostConstruct, PreDestroy 어노테이션 (권장)
 * - 스프링에서 가장 권장하는 방법
 * - 자바 표준의 일부이므로 스프링에 종속되지 않음
 * - 유일한 단점은 코드 수정이 불가능한 외부 라이브러리에 적용 불가(2번 방법 사용)
 *
 * 2. Bean 에 initMethod, destroyMethod 설정 (외부 라이브러리에 사용)
 * - @Bean(initMethod = "init", destroyMethod = "destroy")
 * - destroyMethod 속성에는 추론 기능 있어서 close 또는 shutdown 메서드를 호출해줌
 * - 추론 기능 사용하지 않으려면 명시적으로 빈 문자열 ("")로 설정해야 함
 * 
 * 3. InitializingBean, DisposableBean 인터페이스 구현 (금지)
 * - 코드 수정이 불가능한 외부 라이브러리에 적용 불가
 * - 스프링 전용 인터페이스이므로 스프링에 의존적
 * - 초기화, 소멸 메서드 이름을 변경할 수 없음
 */

public class NetworkClient {

    @Setter
    private String url;

    public NetworkClient() {
        System.out.println("constructor (url:" + url + ")");
    }

    // 서비스 시작 시 호출
    public void connect() {
        System.out.println("connect (url:" + url + ")");
    }

    // 서비스 중에 호출
    public void call(String msg) {
        System.out.println("call (url:" + url + ", msg:" + msg +")");
    }

    // 서비스 종료 시 호출
    public void disconnect() {
        System.out.println("disconnect (url:" + url + ")");
    }

    @PostConstruct
    public void init() throws Exception {
        connect();
        call("init msg");
    }

    @PreDestroy
    public void destroy() throws Exception {
        disconnect();
    }
}

package springbootexercise.shopcore.xmlconfig;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import springbootexercise.shopcore.member.MemberService;
import springbootexercise.shopcore.member.MemberServiceImpl;

public class XmlConfigTest {

    ApplicationContext applicationContext = new GenericXmlApplicationContext("appConfig.xml");

    @Test
    @DisplayName("이름과 Type으로 Bean 조회")
    public void xmlTest() {
        MemberService memberService = applicationContext.getBean("memberService", MemberService.class);
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl.class);
    }
}

package springbootexercise.shopcore.order;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import springbootexercise.shopcore.discount.FixedDiscountPolicy;
import springbootexercise.shopcore.member.*;

class OrderServiceTest {

    MemberService memberService = new MemberServiceImpl(new MemoryMemberRepository());
    OrderService orderService = new OrderServiceImpl(new MemoryMemberRepository(), new FixedDiscountPolicy());

    @Test
    void createOrder() {
        // given
        // primitive 타입은 null값을 가질 수 없어서 Wrapper 타입 씀
        Long memberId = 1L;
        Member member = new Member(memberId, "memberA", Grade.VIP);
        memberService.join(member);

        // when
        Order order = orderService.createOrder(memberId, "itemA", 10000);

        // then
        Assertions.assertThat(order.getDiscountPrice()).isEqualTo(1000);
    }
}

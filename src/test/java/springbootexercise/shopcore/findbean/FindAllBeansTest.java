package springbootexercise.shopcore.findbean;

import org.assertj.core.api.Assert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springbootexercise.shopcore.AppConfig;
import springbootexercise.shopcore.AutoAppConfig;
import springbootexercise.shopcore.discount.DiscountPolicy;
import springbootexercise.shopcore.discount.FixedDiscountPolicy;
import springbootexercise.shopcore.discount.RateDiscountPolicy;
import springbootexercise.shopcore.member.Grade;
import springbootexercise.shopcore.member.Member;
import springbootexercise.shopcore.member.MemberServiceImpl;

import java.util.List;
import java.util.Map;

class FindAllBeansTest {

    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
    // GenericXmlApplicationContext applicationContext = new GenericXmlApplicationContext("appConfig.xml");

    @Test
    @DisplayName("Application Context 직접 순회하며 모든 Bean 출력")
    void findAllBeansManually() {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = applicationContext.getBeanDefinition(beanDefinitionName);
            Object bean = applicationContext.getBean(beanDefinitionName);
            System.out.println("name:" + beanDefinitionName + "\nbean:" + bean + "\nbeanDefinition" + beanDefinition + "\n");
        }
    }

    @Test
    @DisplayName("Application Bean 출력")
    void findAppBeans() {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = applicationContext.getBeanDefinition(beanDefinitionName);
            // BeanDefinition.ROLE_APPLICATION: 사용자가 등록한 Bean
            // BeanDefinition.ROLE_INFRASTRUCTURE: 스프링이 등록한 Bean
            if (beanDefinition.getRole() == BeanDefinition.ROLE_APPLICATION) {
                Object bean = applicationContext.getBean(beanDefinitionName);
                System.out.println("name:" + beanDefinitionName + "\nbean:" + bean + "\nbeanDefinition" + beanDefinition + "\n");
            }
        }
    }

    @Test
    @DisplayName("Type 일치하는 모든 Bean 을 Collection 객체에 자동 주입")
    void addAllBeansToCollection() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DiscountService.class, AutoAppConfig.class);

        DiscountService discountService = applicationContext.getBean(DiscountService.class);
        Member member = new Member(1L, "userA", Grade.VIP);

        int fixedDiscountPrice = discountService.discount(member, 20000, "fixedDiscountPolicy");
        Assertions.assertThat(fixedDiscountPrice).isEqualTo(1000);

        int rateDiscountPrice = discountService.discount(member, 20000, "rateDiscountPolicy");
        Assertions.assertThat(rateDiscountPrice).isEqualTo(2000);
    }

    static class DiscountService {
        private final Map<String, DiscountPolicy> policyMap;
        private final List<DiscountPolicy> policyList;

        @Autowired  // 생성자 하나라서 생략 가능
        public DiscountService(Map<String, DiscountPolicy> policyMap, List<DiscountPolicy> policyList) {
            this.policyMap = policyMap;
            this.policyList = policyList;

            // policyMap = {fixedDiscountPolicy=FixedDiscountPolicy, rateDiscountPolicy=RateDiscountPolicy}
            System.out.println("policyMap = " + policyMap);
            // policyList = [FixedDiscountPolicy, RateDiscountPolicy]
            System.out.println("policyList = " + policyList);

            Assertions.assertThat(policyMap.size()).isEqualTo(2);
            Assertions.assertThat(policyMap.get("fixedDiscountPolicy")).isInstanceOf(FixedDiscountPolicy.class);
            Assertions.assertThat(policyMap.get("rateDiscountPolicy")).isInstanceOf(RateDiscountPolicy.class);
            Assertions.assertThat(policyList.size()).isEqualTo(2);
        }

        public int discount(Member member, int price, String policyName) {
            DiscountPolicy discountPolicy = policyMap.get(policyName);
            return discountPolicy.discount(member, price);
        }
    }

}

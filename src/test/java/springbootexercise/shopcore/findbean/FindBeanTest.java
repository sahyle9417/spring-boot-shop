package springbootexercise.shopcore.findbean;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springbootexercise.shopcore.AppConfig;
import springbootexercise.shopcore.member.MemberService;
import springbootexercise.shopcore.member.MemberServiceImpl;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FindBeanTest {

    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

    @Test
    @DisplayName("Bean 인터페이스 타입으로 조회 (권장)")
    void findBeanByType() {
        MemberService memberService = applicationContext.getBean(MemberService.class);
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl.class);
    }

    @Test
    @DisplayName("Bean 구체 타입으로 조회 (지양)")
    void findBeanByConcreteType() {
        MemberService memberService = applicationContext.getBean(MemberServiceImpl.class);
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl.class);
    }

    @Test
    @DisplayName("Bean 이름으로 조회")
    void findBeanByName() {
        MemberService memberService = applicationContext.getBean("memberService", MemberService.class);
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl.class);
    }

    @Test
    @DisplayName("존재하지 않는 Bean 이름으로 조회")
    void findBeanByNonexistentName() {
        assertThrows(NoSuchBeanDefinitionException.class,
                () -> applicationContext.getBean("nonexistentBeanName", MemberService.class));
    }

}

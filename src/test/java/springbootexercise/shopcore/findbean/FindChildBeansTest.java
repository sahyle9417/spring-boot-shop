package springbootexercise.shopcore.findbean;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springbootexercise.shopcore.discount.DiscountPolicy;
import springbootexercise.shopcore.discount.FixedDiscountPolicy;
import springbootexercise.shopcore.discount.RateDiscountPolicy;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FindChildBeansTest {

    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfig.class);

    @Test
    @DisplayName("부모 Type으로 조회 시, 여러 개의 자식 인스턴스가 있다면 오류 발생")
    void findDupBeansByParentType() {
        assertThrows(NoUniqueBeanDefinitionException.class,
                () -> applicationContext.getBean(DiscountPolicy.class));
    }

    @Test
    @DisplayName("부모 Type으로 조회 시, 여러 개의 자식 인스턴스가 있어도 이름까지 지정하면 정상 조회")
    void findBeanByParentTypeAndUniqueName() {
        DiscountPolicy discountPolicy = applicationContext.getBean("rateDiscountPolicy", DiscountPolicy.class);
        Assertions.assertThat(discountPolicy).isInstanceOf(RateDiscountPolicy.class);
    }

    @Test
    @DisplayName("부모 Type의 모든 Bean 조회하기")
    void findAllBeansByType() {
        Map<String, DiscountPolicy> beansOfType = applicationContext.getBeansOfType(DiscountPolicy.class);
        for (String key: beansOfType.keySet()) {
            System.out.println("key:"+ key + " value:" + beansOfType.get(key));
        }
        Assertions.assertThat(beansOfType.size()).isEqualTo(2);
    }

    @Configuration
    static class TestConfig {
        @Bean
        public DiscountPolicy rateDiscountPolicy() {
            return new RateDiscountPolicy();
        }
        @Bean
        public DiscountPolicy fixedDiscountPolicy() {
            return new FixedDiscountPolicy();
        }
    }
}

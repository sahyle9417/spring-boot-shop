package springbootexercise.shopcore.findbean;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springbootexercise.shopcore.member.MemberRepository;
import springbootexercise.shopcore.member.MemoryMemberRepository;


import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;

class FindSameTypeBeansTest {

    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(DupTypeBeanConfig.class);

    @Test
    @DisplayName("같은 Type의 Bean이 여러 개 있을 때, Type만으로 조회하면 오류")
    void findBeanByDupType() {
        assertThrows(NoUniqueBeanDefinitionException.class,
                () -> applicationContext.getBean(MemberRepository.class));
    }

    @Test
    @DisplayName("같은 Type의 Bean이 여러 개 있어도, Type과 이름으로 조회하면 정상 조회")
    void findBeanByDupTypeAndUniqueName() {
        MemberRepository memberRepository = applicationContext.getBean("memberRepository1", MemberRepository.class);
        Assertions.assertThat(memberRepository).isInstanceOf(MemoryMemberRepository.class);
    }

    @Test
    @DisplayName("특정 Type의 모든 Bean 조회하기")
    void findAllBeansByType() {
        Map<String, MemberRepository> beansOfType = applicationContext.getBeansOfType(MemberRepository.class);
        for (String key: beansOfType.keySet()) {
            System.out.println("key:"+ key + " value:" + beansOfType.get(key));
        }
        Assertions.assertThat(beansOfType.size()).isEqualTo(2);
    }


    @Configuration
    static class DupTypeBeanConfig {
        @Bean
        public MemberRepository memberRepository1() {
            return new MemoryMemberRepository();
        }

        @Bean
        public MemberRepository memberRepository2() {
            return new MemoryMemberRepository();
        }

    }
}

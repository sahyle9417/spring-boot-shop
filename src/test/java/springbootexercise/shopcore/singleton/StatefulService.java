package springbootexercise.shopcore.singleton;

public class StatefulService {

    // 잘못된 예시 (상태를 유지하는 bean)
    // spring bean 무조건 상태 없이 동작해야 함
    private int price;

    public void order(String name, int price) {
        System.out.println("name:" + name + " price:" + price);
        // 여기가 문제
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}

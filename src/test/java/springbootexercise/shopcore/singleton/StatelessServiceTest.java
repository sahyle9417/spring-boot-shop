package springbootexercise.shopcore.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class StatelessServiceTest {

    @Test
    void statefulServiceSingleton() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfig.class);

        // 기본적으로 spring bean은 singleton 이므로 아래 2개의 인스턴스는 아예 동일함
        StatelessService statelessService1 = applicationContext.getBean(StatelessService.class);
        StatelessService statelessService2 = applicationContext.getBean(StatelessService.class);

        // 사용자1 주문 금액 : 10000, 사용자2 주문 금액 : 20000
        int price1 = statelessService1.order("user1", 10000);
        int price2 = statelessService2.order("user2", 20000);

        // stateless -> 올바른 결과 도출
        assertThat(price1).isEqualTo(10000);
        assertThat(price2).isEqualTo(20000);
    }

    static class TestConfig {
        @Bean
        public StatelessService statelessService() {
            return new StatelessService();
        }

    }

}
package springbootexercise.shopcore.singleton;

public class SingletonService {

    // 클래스가 메모리에 올라갈 때, static 영역에 인스턴스를 딱 1개만 생성하고, 유일한 인스턴스를 자기 자신의 멤버변수에 넣는다.
    public static final SingletonService instance = new SingletonService();

    // public 선언하여 인스턴스가 필요한 경우 이 static 메서드를 통해서만 조회할 수 있도록 허용한다.
    public static SingletonService getInstance() {
        return instance;
    }

    // 생성자를 private 선언하여 외부에서 생성자를 실행할 수 없게 막는다. 즉, 외부에서 인스턴스를 생성할 수 없다.
    private SingletonService() {
    }

}

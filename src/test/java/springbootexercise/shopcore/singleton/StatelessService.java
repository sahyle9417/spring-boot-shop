package springbootexercise.shopcore.singleton;

public class StatelessService {

    // 올바른 예시 (상태를 유지하지 않는 bean)
    // spring bean 무조건 상태 없이 동작해야 함
    public int order(String name, int price) {
        System.out.println("name:" + name + " price:" + price);
        return price;
    }

}

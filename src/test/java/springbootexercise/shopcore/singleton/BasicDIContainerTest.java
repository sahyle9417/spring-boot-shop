package springbootexercise.shopcore.singleton;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springbootexercise.shopcore.AppConfig;
import springbootexercise.shopcore.member.MemberService;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BasicDIContainerTest {

    @Test
    @DisplayName("Spring이 아닌 기본적인 DI Container")
    void basicDIContainer() {
        AppConfig appConfig = new AppConfig();
        MemberService memberService1 = appConfig.memberService();
        MemberService memberService2 = appConfig.memberService();
        System.out.println("memberService1:" + memberService1);
        System.out.println("memberService2:" + memberService2);
        assertThat(memberService1).isNotSameAs(memberService2);
    }

    @Test
    @DisplayName("singleton 테스트")
    void singletonServiceTest() {
        // 'SingletonService()' has private access
        // new SingletonService();

        SingletonService singletonService1 = SingletonService.getInstance();
        SingletonService singletonService2 = SingletonService.getInstance();

        System.out.println("singletonService1:" + singletonService1);
        System.out.println("singletonService2:" + singletonService2);
        assertThat(singletonService1).isSameAs(singletonService2);
    }

    @Test
    @DisplayName("spring singleton 테스트")
    void springSingletonServiceTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        MemberService memberService1 = applicationContext.getBean(MemberService.class);
        MemberService memberService2 = applicationContext.getBean(MemberService.class);

        System.out.println("memberService1:" + memberService1);
        System.out.println("memberService2:" + memberService2);
        assertThat(memberService1).isSameAs(memberService2);
    }
}

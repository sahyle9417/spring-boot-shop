package springbootexercise.shopcore.singleton;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springbootexercise.shopcore.AppConfig;
import springbootexercise.shopcore.member.MemberRepository;
import springbootexercise.shopcore.member.MemberServiceImpl;
import springbootexercise.shopcore.order.OrderServiceImpl;

public class ConfigSingletonTest {

    @Test
    void configSingletonTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

        MemberServiceImpl memberService = applicationContext.getBean("memberService", MemberServiceImpl.class);
        OrderServiceImpl orderService = applicationContext.getBean("orderService", OrderServiceImpl.class);

        MemberRepository memberRepository1 = memberService.getMemberRepository();
        MemberRepository memberRepository2 = orderService.getMemberRepository();
        MemberRepository memberRepository3 = applicationContext.getBean("memberRepository", MemberRepository.class);

        System.out.println("memberRepository1: " + memberRepository1);
        System.out.println("memberRepository2: " + memberRepository2);
        System.out.println("memberRepository3: " + memberRepository3);

        Assertions.assertThat(memberRepository1).isSameAs(memberRepository2);
        Assertions.assertThat(memberRepository1).isSameAs(memberRepository3);
        Assertions.assertThat(memberRepository2).isSameAs(memberRepository3);
    }

    @Test
    void configManipulateTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        AppConfig appConfig = applicationContext.getBean(AppConfig.class);
        // appConfig Type: class AppConfig$$EnhancerBySpringCGLIB$$dd9e2462
        // Configuration 어노테이션 붙이면 바이트코드를 조작하는 CGLIB 기술 사용해서 Singleton 보장
        // Configuration 클래스를 직접 생성하지 않고 해당 클래스의 자식 클래스를 생성
        // 이를 통해 특정 Bean 생성자가 중복 호출되더라도 해당 Bean이 이미 생성되어 있다면 재생성하지 않음
        // 만약 Configuration 어노테이션 없이 Bean 어노테이션만 사용하면 Singleton 깨짐
        System.out.println("appConfig Type: " + appConfig.getClass());
    }
}

package springbootexercise.shopcore.singleton;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class StatefulServiceTest {

    @Test
    void statefulServiceSingleton() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfig.class);

        // 기본적으로 spring bean은 singleton 이므로 아래 2개의 인스턴스는 아예 동일함
        StatefulService statefulService1 = applicationContext.getBean(StatefulService.class);
        StatefulService statefulService2 = applicationContext.getBean(StatefulService.class);

        // 사용자1 주문 금액 : 10000, 사용자2 주문 금액 : 20000
        statefulService1.order("user1", 10000);
        statefulService2.order("user2", 20000);

        // singleton 이므로 사용자2가 state 바꿔버려서 사용자1의 주문금액이 덮어쓰기 되어 버림
        int price1 = statefulService1.getPrice();
        int price2 = statefulService2.getPrice();

        // stateful -> 잘못된 결과 도출
        assertThat(price1).isEqualTo(20000);
        assertThat(price2).isEqualTo(20000);
    }

    static class TestConfig {
        @Bean
        public StatefulService statefulService() {
            return new StatefulService();
        }

    }

}
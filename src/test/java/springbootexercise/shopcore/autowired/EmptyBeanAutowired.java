package springbootexercise.shopcore.autowired;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.lang.Nullable;
import springbootexercise.shopcore.filter.MyExcludeBean;
import springbootexercise.shopcore.member.Member;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class EmptyBeanAutowired {

    @Test
    void emptyBeanAutowired() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(EmptyBeanAutowiredConfig.class);
    }

    static class EmptyBeanAutowiredConfig {

        // Member 타입의 Bean 존재하지 않으므로 required 였다면 Exception 발생
        // 여기서는 required 값이 false 이므로 해당 메서드가 실행되지 않음
        @Autowired(required = false)
        public void nonRequiredBean(Member nonRequiredBean) {
            System.out.println("nonRequiredBean:" + nonRequiredBean);
        }

        // 매개변수가 Nullable 인 경우 Exception 발생하지 않고 null 입력됨
        // required 값은 true 이므로 해당 메서드 실행은 됨
        @Autowired
        public void nullableBean(@Nullable Member nullableBean) {
            System.out.println("nullableBean:" + nullableBean);
        }

        // 매개변수가 Optional 인 경우 Exception 발생하지 않고 Optional.empty 입력됨
        // required 값은 true 이므로 해당 메서드 실행은 됨
        @Autowired
        public void optionalBean(Optional<Member> optionalBean) {
            System.out.println("optionalBean:" + optionalBean);
        }

    }
}

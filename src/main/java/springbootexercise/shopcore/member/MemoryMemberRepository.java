package springbootexercise.shopcore.member;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

// Bean 이름은 클래스명 맨 앞글자만 소문자로 바꿔서 사용한다.
// 단 Component 태그 안에 Bean 이름을 명세하여 수정할 수 있다.
@Component
public class MemoryMemberRepository implements MemberRepository{

    private static Map<Long, Member> store = new HashMap<>();

    @Override
    public void save(Member member) {
        store.put(member.getId(), member);
    }

    @Override
    public Member findById(Long memberId) {
        return store.get(memberId);
    }
}

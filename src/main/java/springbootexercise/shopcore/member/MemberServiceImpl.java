package springbootexercise.shopcore.member;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


/*
 * Component 어노테이션
 * 클래스명 맨 앞글자만 소문자로 바꾼 Bean 이름으로 등록
 * 단 Component 태그 안에 Bean 이름을 명세하여 수정할 수 있다.
 *
 * RequiredArgsConstructor 어노테이션
 * 반드시 생성자에서 입력받아야 하는 final 멤버변수를 주입받는 생성자를 만들어줌
 */

@Component
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

    // 생성자 주입 + final 키워드 -> 불변 보장
    private final MemberRepository memberRepository;

    /*
     * 주입방법1. 생성자 주입 (권장)
     * 생성자에 @Autowired 붙임
     * @Autowired: applicationContext.getBean() 수행
     * 생성자 주입의 장점 2가지
     * 1) final 키워드를 적용하여 불변을 보장
     * 2) 생성과 동시에 의존하는 Bean 세팅되는 것을 보장
     * (의존하는 Bean 없다면 Bean 생성 시 컴파일 오류 발생)
     * (setter 주입은 런타임 오류 발생 가능)
     * 생성자가 1개만 있을 땐 @Autowired 생략해도 자동으로 @Autowired 적용됨
     */
//    @Autowired
//    public MemberServiceImpl(MemberRepository memberRepository) {
//        this.memberRepository = memberRepository;
//    }

    /*
     * 주입방법2. setter 주입 (권장X)
     * setter에 @Autowired 붙임
     * 수동 주입해야하는게 아니라면 쓰지마
     */
//     @Autowired
//     public void setMemberRepository(MemberRepository memberRepository) {
//         this.memberRepository = memberRepository;
//     }

    /*
     * 주입방법3. 필드 주입 (금지)
     * 필드에 @Autowired 붙임
     * 외부에서 변경할 수 없어 테스트 하기 어렵다, 테스트 코드에서만 사용
     */
//     @Autowired
//     private MemberRepository memberRepository;

    @Override
    public void join(Member member) {
        memberRepository.save(member);
    }

    @Override
    public Member findMember(Long memberId) {
        return memberRepository.findById(memberId);
    }

    // 테스트 용도
    public MemberRepository getMemberRepository() {
        return memberRepository;
    }
}

package springbootexercise.shopcore;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

// Config 클래스 + ComponentScan 작동 방식
// 현재 위치의 하위 경로를 순회하며, 아래의 어노테이션 클래스를 Bean 등록 (내부에 Component 붙어있음)
// ComponentScan 대상: Component, Controller, Service, Repository, Configuration
// Controller: MVC 패턴의 컨트롤러 역할 수행
// Repository: 데이터 접근 계층, 데이터 계층 예외를 스프링 예외로 변환하여 유연한 catch 가능
// Configuration: Singleton Bean 생성, 관리
// Bean 이름은 클래스명 맨 앞글자만 소문자로 바꿔서 사용한다.
// 단 Component 태그 안에 Bean 이름을 명세하여 수정할 수 있다.
// basePackages 사용하여 순회할 경로 변경할 수 있다.
// AutoWired 어노테이션을 참조해서 Bean 간의 DI를 수행한다.
// 아래 예제에서 ComponentScan 대상에서 Configuration 어노테이션을 제외한 이유
// Config 클래스를 참조하지 않고, ComponentScan 만으로 Bean 등록되는지 확인하기 위함
@Configuration
@ComponentScan(
        excludeFilters = @ComponentScan.Filter(
                type = FilterType.ANNOTATION, classes = Configuration.class
        )
)
public class AutoAppConfig {

}

package springbootexercise.shopcore.requestscope;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyService {

    /*
     * singleton Bean 안에서 request scope Bean 사용하는 방법 (객체 조회 필요할 때까지 객체 생성 지연)
     *
     * 1. ScopedProxyMode (권장)
     * - request scope Bean 에 proxyMode = ScopedProxyMode.TARGET_CLASS 설정
     * - 바이트코드 조작 라이브러리(CGLIB) 사용
     * - request scope Bean 상속한 가짜 프록시 Bean(MyLogger$$EnhancerBySpringCGLIB) 생성하여 주입
     * - 가짜 프록시 객체는 singleton 처럼 존재하다가 실제 요청이 오면 그때 내부에서 실제 Bean 요청
     * - 사용하는 코드에서는 불필요한 코드 없이 그냥 주입받아서 쓰면 됨
     *
     * 2. ObjectProvider
     * - ObjectProvider 주입받아놨다가 사용할 때 getObject 메서드 호출
     * - 사용하는 코드에서 바로 주입받지 못하고 Provider 에게서 받아와야 함
     */

    // ObjectProvider 방식은 request scope Bean (MyLogger) 을 제공하는 Provider 필요
    // private final ObjectProvider<MyLogger> myLoggerProvider;

    // ScopedProxyMode 방식은 그냥 쓰면 됨
    // 실제로는 가짜 프록시 Bean 이 주입된다 (MyLogger$$EnhancerBySpringCGLIB)
    private final MyLogger myLogger;

    public void logic() {

        // ObjectProvider 방식은 아래와 같이 Provider 에게서 받아오는 코드 필요
        // MyLogger myLogger = myLoggerProvider.getObject();

        // ScopedProxyMode 방식은 그냥 쓰면 됨
        // 실제로는 가짜 프록시 Bean 주입되어 있음 (MyLogger$$EnhancerBySpringCGLIB)
        myLogger.log("service");
    }
}

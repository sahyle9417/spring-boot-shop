package springbootexercise.shopcore.requestscope;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequiredArgsConstructor
public class MyController {

    /*
     * singleton Bean 안에서 request scope Bean 사용하는 방법
     *
     * 1. ScopedProxyMode (권장)
     * - request scope Bean 에 proxyMode = ScopedProxyMode.TARGET_CLASS 설정
     * - 바이트코드 조작 라이브러리(CGLIB) 사용
     * - request scope Bean 상속한 가짜 프록시 Bean 생성하여 주입
     * - 사용하는 코드에서는 불필요한 코드 없이 그냥 주입받아서 쓰면 됨
     *
     * 2. ObjectProvider
     * - ObjectProvider 주입받아놨다가 사용할 때 getObject 메서드 호출
     * - 사용하는 코드에서 바로 주입받지 못하고 Provider 에게서 받아와야 함
     */

    // ObjectProvider 방식은 request scope Bean (MyLogger) 을 제공하는 Provider 필요
    // private final ObjectProvider<MyLogger> myLoggerProvider;

    // ScopedProxyMode 방식은 그냥 쓰면 됨
    // 실제로는 가짜 프록시 Bean 이 주입된다 (MyLogger$$EnhancerBySpringCGLIB)
    private final MyLogger myLogger;
    private final MyService myService;

    @RequestMapping("log")
    @ResponseBody
    public String log(HttpServletRequest request) throws InterruptedException {
        String requestUrl = request.getRequestURL().toString();

        // ObjectProvider 방식은 아래와 같이 Provider 에게서 받아오는 코드 필요
        // MyLogger myLogger = myLoggerProvider.getObject();

        // ScopedProxyMode 방식은 그냥 쓰면 됨
        // 실제로는 가짜 프록시 Bean 주입되어 있음 (MyLogger$$EnhancerBySpringCGLIB)
        myLogger.setRequestUrl(requestUrl);
        myLogger.log("controller");

        Thread.sleep(1000);

        myService.logic();

        return "OK";
    }

}

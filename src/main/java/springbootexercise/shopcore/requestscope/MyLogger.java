package springbootexercise.shopcore.requestscope;

import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.UUID;

// 같은 요청 Thread 내에서 동일한 request scope Bean 공유
// 즉, Controller 안에 MyLogger 와 Service 안에 MyLogger 은 동일
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MyLogger {

    private String uuid;

    @Setter
    private String requestUrl;

    public void log(String msg) {
        System.out.println("[" + uuid + "][" + requestUrl + "][" + msg + "]");
    }

    @PostConstruct
    public void init() {
        System.out.println("MyLogger.init");
        uuid = UUID.randomUUID().toString();
    }

    @PreDestroy
    public void destroy() {
        System.out.println("MyLogger.destroy");
    }

}

package springbootexercise.shopcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springbootexercise.shopcore.discount.DiscountPolicy;
import springbootexercise.shopcore.discount.RateDiscountPolicy;
import springbootexercise.shopcore.member.MemberRepository;
import springbootexercise.shopcore.member.MemberService;
import springbootexercise.shopcore.member.MemberServiceImpl;
import springbootexercise.shopcore.member.MemoryMemberRepository;
import springbootexercise.shopcore.order.OrderService;
import springbootexercise.shopcore.order.OrderServiceImpl;

@Configuration
public class AppConfig {

    @Bean
    public MemberRepository memberRepository() {
        return new MemoryMemberRepository();
    }

    @Bean
    public DiscountPolicy discountPolicy() {
        return new RateDiscountPolicy();
    }

    @Bean
    public MemberService memberService() {
        return new MemberServiceImpl(memberRepository());
    }

    @Bean
    public OrderService orderService() {
        return new OrderServiceImpl(memberRepository(), discountPolicy());
    }
}

package springbootexercise.shopcore.order;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import springbootexercise.shopcore.annotation.MainDiscountPolicy;
import springbootexercise.shopcore.discount.DiscountPolicy;
import springbootexercise.shopcore.member.Member;
import springbootexercise.shopcore.member.MemberRepository;


/*
 * Component 어노테이션
 * 클래스명 맨 앞글자만 소문자로 바꾼 Bean 이름으로 등록
 * 단 Component 태그 안에 Bean 이름을 명세하여 수정할 수 있다.
 *
 * RequiredArgsConstructor 어노테이션
 * 반드시 생성자에서 입력받아야 하는 final 멤버변수를 주입받는 생성자를 만들어줌
 */

@Component
@MainDiscountPolicy
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    // 생성자 주입 + final 키워드 -> 불변 보장
    private final MemberRepository memberRepository;
    /*
     * 만약 DiscountPolicy를 구현한 객체 Bean 이 여러 개면 주입할 때 중복 Bean 오류 (Not Unique Bean 오류) 발생
     * 
     * No Unique Bean 오류 회피 방법 4가지
     * 1. Primary: Primary 어노테이션이 붙은 Bean 을 가장 높은 우선순위로 설정 (다른 Bean 무시)
     * 2. Qualifier: 구분하기 위한 추가 구분자 제공 (Qualifier 간 매칭 수행)
     * 3. 커스텀 Annotation 생성: 2번의 Qualifier 와 유사하지만 오기입에 의한 런타임 오류를 방지할 수 있어서 권장
     * 4. Autowired + 이름 매칭: bean 이름과 필드명 일치시킴
     * (Primary, Qualifier 중에서는 명시적으로 지정하는 Qualifier 우선순위가 더 높다)
     *
     * 권장하는 방법
     * 1. 메인 Bean: Primary 붙여서 주로 사용
     * 2. 서브 Bean: 커스텀 Annotation 또는 Qualifier 붙여서 필요 시 명시적으로 적용해서 사용
     */
    private final DiscountPolicy discountPolicy;

     /*
      * 주입방법1. 생성자 주입 (권장)
      * 생성자에 @Autowired 붙임
      * @Autowired: applicationContext.getBean() 수행
      * 생성자 주입의 장점 2가지
      * 1) final 키워드를 적용하여 불변을 보장
      * 2) 생성과 동시에 의존하는 Bean 세팅되는 것을 보장
      * (의존하는 Bean 없다면 Bean 생성 시 컴파일 오류 발생)
      * (setter 주입은 런타임 오류 발생 가능)
      * 생성자가 1개만 있을 땐 @Autowired 생략해도 자동으로 @Autowired 적용됨
      */
//    @Autowired
//    public OrderServiceImpl(MemberRepository memberRepository, @MainDiscountPolicy DiscountPolicy discountPolicy) {
//        this.memberRepository = memberRepository;
//        this.discountPolicy = discountPolicy;
//    }

    /*
     * 주입방법2. setter 주입 (권장X)
     * setter에 @Autowired 붙임
     * 수동 주입해야하는게 아니라면 쓰지마
     */
//     @Autowired
//     public void setMemberRepository(MemberRepository memberRepository) {
//         this.memberRepository = memberRepository;
//     }
//     @Autowired
//     public void setDiscountPolicy(@MainDiscountPolicy DiscountPolicy discountPolicy) {
//         this.discountPolicy = discountPolicy;
//     }

    /*
     * 주입방법3. 필드 주입 (금지)
     * 필드에 @Autowired 붙임
     * 외부에서 변경할 수 없어 테스트 하기 어렵다, 테스트 코드에서만 사용
     */
//     @Autowired
//     private MemberRepository memberRepository;
//     @Autowired
//     private DiscountPolicy discountPolicy;

    @Override
    public Order createOrder(Long memberId, String itemName, int itemPrice) {
        Member member = memberRepository.findById(memberId);
        int discountPrice = discountPolicy.discount(member, itemPrice);
        return new Order(memberId, itemName,itemPrice, discountPrice);
    }

    // 테스트 용도
    public MemberRepository getMemberRepository() {
        return memberRepository;
    }
}

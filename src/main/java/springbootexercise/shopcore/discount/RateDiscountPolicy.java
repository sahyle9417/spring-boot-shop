package springbootexercise.shopcore.discount;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springbootexercise.shopcore.annotation.MainDiscountPolicy;
import springbootexercise.shopcore.member.Grade;
import springbootexercise.shopcore.member.Member;

/*
 * Component
 * 클래스명 맨 앞글자만 소문자로 바꿔서 Bean 으로 등록
 * Component 태그 안에 Bean 이름을 명세하여 수정할 수 있다.
 *
 * No Unique Bean 오류 회피 방법 4가지
 * 1. Primary: Primary 어노테이션이 붙은 Bean 을 가장 높은 우선순위로 설정 (다른 Bean 무시)
 * 2. Qualifier: 구분하기 위한 추가 구분자 제공 (Qualifier 간 매칭 수행)
 * 3. 커스텀 Annotation 생성: 2번의 Qualifier 와 유사하지만 오기입에 의한 런타임 오류를 방지할 수 있어서 권장
 * 4. Autowired + 이름 매칭: bean 이름과 필드명 일치시킴
 * (Primary, Qualifier 중에서는 명시적으로 지정하는 Qualifier 우선순위가 더 높다)
 */

@Component
@Primary
@MainDiscountPolicy // 동일 Type 의 여러 Bean 중에 어떤 것을 주입할지 구분하기 위한 커스텀 어노테이션
public class RateDiscountPolicy implements DiscountPolicy {

    private final int discountPercent = 10;

    @Override
    public int discount(Member member, int price) {
        if (member.getGrade() == Grade.VIP) {
            return price * discountPercent / 100;
        } else {
            return 0;
        }
    }
}

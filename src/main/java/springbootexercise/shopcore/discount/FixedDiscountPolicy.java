package springbootexercise.shopcore.discount;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import springbootexercise.shopcore.member.Grade;
import springbootexercise.shopcore.member.Member;

@Component
@Qualifier("subDiscountPolicy")
public class FixedDiscountPolicy implements DiscountPolicy{

    int fixedDiscountAmt = 1000;

    @Override
    public int discount(Member member, int price) {
        if (member.getGrade() == Grade.VIP) {
            return fixedDiscountAmt;
        }
        else{
            return 0;
        }
    }
}

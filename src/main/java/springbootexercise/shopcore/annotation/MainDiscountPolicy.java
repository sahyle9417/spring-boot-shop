package springbootexercise.shopcore.annotation;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Qualifier("mainDiscountPolicy")
public @interface MainDiscountPolicy {
    /**
     * 기존 Qualifier 역할을 수행하는 커스텀 어노테이션
     * Qualifier 에 잘못된 문자열 지정 시 런타임 오류 발생하는데 반해
     * 커스텀 어노테이션 사용 시 컴파일 오류로 런타임 오류를 방지할 수 있음
     */
}
